# Cocktail Solution

This is a commandline python application that accepts cocktail ingredients as inputs and outputs a list of cocktails 
that can be made with those.

# Getting Started

1. Clone this repo.
2. Start a virtual environment (recommended but optional).
3. Install the requirements using pip: `pip install -r requirements.txt`.
4. Open terminal and run the script.

# Usage

Invoke the `main.py` script with python and pass in your ingredients as comma separated values as an argument. For example:
```
python main.py gin,vodka
```
Note that there can be no spaces between your comma separated ingredients.

This script limits the results to 5. You can change this limit by passing in a limit parameter as a third argument as follows:
```
python main.py gin,vodka 10
```

# Testing

Install dev requirements from `requirements-dev.txt` using:
```
pip install -r requirements-dev.txt
```

To run unit tests, use:
```
pytest test_unit.py
```

To run integration test, use:
```
pytest test_integration.py
```

## Authors and acknowledgment
This application was written by Shah, engineer at Unai.

## License
This application has not been licensed.

## Project status
This implementation is complete.

Hi! Your specified ingredients are: gin, vodka.

There are 100 drinks that can be made from your ingredients.

Lets have a look at the first 5 (Note that you can change this by passing a second limit parameter as an argument).

3-Mile Long Island Iced Tea https://www.thecocktaildb.com/images/media/drink/rrtssw1472668972.jpg

Ingredients required:
Gin - 1/2 oz
Light rum - 1/2 oz
Tequila - 1/2 oz
Triple sec - 1/2 oz
Vodka - 1/2 oz
Coca-Cola - 1/2 oz
Sweet and sour - 1-2 dash 
Bitters - 1 wedge 
Lemon - Garnish with


69 Special https://www.thecocktaildb.com/images/media/drink/vqyxqx1472669095.jpg

Ingredients required:
Gin - 2 oz dry 
7-Up - 4 oz 
Lemon juice - 0.75 oz 


A1 https://www.thecocktaildb.com/images/media/drink/2x8thr1504816928.jpg

Ingredients required:
Gin - 1 3/4 shot 
Grand Marnier - 1 Shot 
Lemon Juice - 1/4 Shot
Grenadine - 1/8 Shot


Abbey Cocktail https://www.thecocktaildb.com/images/media/drink/mr30ob1582479875.jpg

Ingredients required:
Gin - 1 1/2 oz 
Orange bitters - 1 dash 
Orange - Juice of 1/4 
Cherry - 1 


Abbey Martini https://www.thecocktaildb.com/images/media/drink/2mcozt1504817403.jpg

Ingredients required:
Gin - 2 shots 
Sweet Vermouth - 1 shot 
Orange Juice - 1 shot 
Angostura Bitters - 3 dashes 


Have fun!

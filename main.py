"""
This module takes commandline arguments for ingredients as inputs and prints out
the cocktails that can be made with those ingredients.
"""

import sys
from typing import Any, Dict, List

import requests


BASE_URL="https://www.thecocktaildb.com/api/json/v1/1/"

ingredients = sys.argv[1].split(',')
try:
    limit = sys.argv[2]
except IndexError:
    limit = 5

def get_cocktails_by_integredients(ingredients: List) -> requests.Response:
    """
    Wrapper for cocktailDB cocktail search endpoint.
    ingredients: list of ingredients
    """
    return requests.get(
        f"{BASE_URL}filter.php",
        params={
            "i": ",".join(ingredients).rstrip(","),
    })


def get_ingredients_by_cocktail(drink: str) -> requests.Response:
    """
    Wrapper for cocktailDB ingredient search endpoint.
    drink: title of the drink
    """
    x = requests.get(
           f"{BASE_URL}search.php",
            params={
                "s": drink
            }
        )
    return x

def parse_ingredients(response: Dict) -> None:
    """
    Takes ingredients response from cocktailDB endpoint
    and prints out one per line.
    response: a dict containing the response from cocktailDB endpoint
    """
    print("Ingredients required:")
    i = 1
    while(i<=15): # Considering no drink in cocktailDB has more than 15 ingredients
        if response["drinks"][0][f'strIngredient{str(i)}']:
            print(
                f'{response["drinks"][0][f"strIngredient{str(i)}"]} - {response["drinks"][0][f"strMeasure{str(i)}"]}'
            ) 
        i = i + 1
    print("\n")


def parse_drinks(response: Dict, limit: Any) -> None:
    """
    Filters drinks by limit specified and prints out along
    with ingredients.
    response: a dict containing the response from cocktailDB endpoint
    limit: limit for the number of results
    """
    if int(limit) > len(response['drinks']):
        filtered = response['drinks']
    else:
        filtered = response['drinks'][:int(limit)]
    for drink in filtered:
        print(f"{drink['strDrink']} {drink['strDrinkThumb']}\n")
        x = get_ingredients_by_cocktail(drink['strDrink'])
        parse_ingredients(x.json())

def main(ingredients: List, limit: Any) -> None:
    """
    Looks up drinks and ingredients required based on 
    the ingredients provided and filters by limit.
    ingredients: a list of ingredients to look up cocktails
    limit: a number for limiting the results
    """
    print(
        f"Hi! Your specified ingredients are: {', '.join(ingredients).rstrip(', ')}.\n"
        )

    x = get_cocktails_by_integredients(ingredients)

    if x.status_code != 200:
        print("Sorry, couldn't connect to the Cocktail DB API")
    else:
        while(not x.text and ingredients):
            del ingredients[-1] # If no cocktails are found, we decrease the ingredients one by one
            x = get_cocktails_by_integredients(ingredients)
        if x.text and x.json().get("drinks"):
            print(f"There are {len(x.json()['drinks'])} drinks that can be made from your ingredients.\n")
            print(f"Lets have a look at the first {str(limit)} (Note that you can change this by passing a second limit parameter as an argument).\n")
            parse_drinks(x.json(), limit)
            print("Have fun!")
        else:
            print("Couldn't find any cocktails for the ingredients you have specified, sorry!")

if __name__ == "__main__":
    main(ingredients, limit)

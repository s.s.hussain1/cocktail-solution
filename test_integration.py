"""
Integration tests testing against the cocktailDB API.
"""
import pytest

@pytest.mark.parametrize(
    'printed_output_file_location, ingredients, limit',
    [
       (
          'test_data/text/real_scenario_printed_output.txt',
          "gin,vodka",
          None
       ),
        (
          'test_data/text/real_scenario_printed_output_limit_3.txt',
          "gin,vodka",
          str(3)
       ),
    (
          'test_data/text/real_scenario_printed_output_limit_10.txt',
          "gin,vodka",
          str(10)
       ),
           (
          'test_data/text/real_scenario_printed_output_no_drinks.txt',
          "wrongingredient",
          None
       )
    ]
)
def test_main(printed_output_file_location, ingredients, limit):
    with open(printed_output_file_location, 'r') as file:
        data = file.read()
    from subprocess import PIPE, run
    if limit:
        result = run(["python3", "main.py", ingredients, limit], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    else:
        result = run(["python3", "main.py", ingredients], stdout=PIPE, stderr=PIPE, universal_newlines=True)
    assert  result.stdout == data

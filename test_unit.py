"""
Unit tests for main testing against mocked endpoints.
"""

import io
import sys
import json
import pytest

from unittest.mock import patch

from main import (
    get_cocktails_by_integredients, 
    get_ingredients_by_cocktail, 
    parse_drinks, 
    parse_ingredients, 
    main
)


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data

    @property
    def text(self):
        return json.dumps(self.json_data)


with open('test_data/json/drinks_happy_path.json', 'r') as file:
        drinks_happy_path_json = file.read()

with open('test_data/json/drinks_no_drinks.json', 'r') as file:
        drinks_no_drinks_json = file.read()

with open('test_data/json/ingredients_happy_path.json', 'r') as file:
        ingredients_happy_path_json = file.read()

with open('test_data/json/ingredients_no_ingredients.json', 'r') as file:
        ingredients_no_ingredients_json = file.read()

with open('test_data/text/ingredients_printed_output.txt', 'r') as file:
        ingredients_printed_output_text = file.read()

with open('test_data/text/no_ingredients_printed_output.txt', 'r') as file:
        no_ingredients_printed_output_text = file.read()

with open('test_data/text/parse_drinks_printed_output_2.txt', 'r') as file:
        parse_drinks_printed_output_2_drinks = file.read()

with open('test_data/text/parse_drinks_printed_output_3.txt', 'r') as file:
        parse_drinks_printed_output_all_drinks = file.read()

with open('test_data/text/main_dummy_ingredients_printed_output.txt', 'r') as file:
        main_dummy_ingredients_printed_output = file.read()

@pytest.mark.parametrize(
    'mocked_response',
    [
        MockResponse(json.loads(drinks_happy_path_json), 200),
        MockResponse(json.loads(drinks_no_drinks_json), 404),
        MockResponse({}, 400)
    ]
)
@patch('requests.get')
def test_cocktails_are_returned_by_ingredients(requests_get, mocked_response):
    requests_get.return_value = mocked_response
    assert get_cocktails_by_integredients('gin,vodka') == mocked_response

@pytest.mark.parametrize(
    'mocked_response',
    [
        MockResponse(json.loads(ingredients_happy_path_json), 200),
        MockResponse(json.loads(ingredients_no_ingredients_json), 200),
        MockResponse({}, 400)
    ]
)
@patch('requests.get')
def test_ingredients_are_returned_by_cocktail(requests_get, mocked_response):
    patched_response = mocked_response
    requests_get.return_value = patched_response
    assert get_ingredients_by_cocktail('drink1') == patched_response

@pytest.mark.parametrize(
    'ingredients, printed_output',
    [
        (
            json.loads(ingredients_happy_path_json),
            ingredients_printed_output_text
        ),
        (
            json.loads(ingredients_no_ingredients_json),
            no_ingredients_printed_output_text
        ),

    ]
)
def test_parse_ingredients(ingredients, printed_output):
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    parse_ingredients(ingredients)
    sys.stdout = sys.__stdout__   # Reset redirect.
    assert capturedOutput.getvalue() == printed_output


def mocked_parse_ingredients(*args, **kwargs):
    print(ingredients_printed_output_text)

@pytest.mark.parametrize(
    'mocked_get_ingredients_by_cocktail, drinks, limit, printed_output',
    [
       (
          MockResponse(json.loads(ingredients_happy_path_json), 200),
          json.loads(drinks_happy_path_json),
          2,
          parse_drinks_printed_output_2_drinks
       ),
        (
          MockResponse(json.loads(ingredients_happy_path_json), 200),
          json.loads(drinks_happy_path_json),
          5,
          parse_drinks_printed_output_all_drinks
       )
    ]
)
@patch("main.parse_ingredients", side_effect=mocked_parse_ingredients)
@patch("main.get_ingredients_by_cocktail")
def test_parse_drinks(get_ingredients_by_cocktail, parse_ingredients, mocked_get_ingredients_by_cocktail, drinks, limit, printed_output):
    get_ingredients_by_cocktail.return_value = mocked_get_ingredients_by_cocktail
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    parse_drinks(drinks, limit)
    sys.stdout = sys.__stdout__   # Reset redirect.
    assert capturedOutput.getvalue() == printed_output

def mocked_parse_drinks(*args, **kwargs):
    print("Here are the ingredients:\na - 1 oz\nb - 1 oz\n")

@pytest.mark.parametrize(
    'mocked_get_cocktails_by_ingredients, input_args, limit, printed_output',
    [
       (
          MockResponse(json.loads(drinks_happy_path_json), 200),
          ['gin','vodka'],
          5,
          main_dummy_ingredients_printed_output
       ),
       (
          MockResponse({}, 400),
          ['gin','vodka'],
          5,
          (
            "Hi! Your specified ingredients are: gin, vodka.\n"
            "\nSorry, couldn't connect to the Cocktail DB API\n"
          )
       ),
        (
          MockResponse({}, 200),
          ['gin','vodka'],
          5,
          (
            "Hi! Your specified ingredients are: gin, vodka.\n\n"
            "Couldn't find any cocktails for the ingredients you have specified, sorry!\n"
          )
       )
    ]
)
@patch("main.get_cocktails_by_integredients")
@patch("main.parse_drinks", side_effect=mocked_parse_drinks)
def test_main(
    parse_drinks, 
    get_cocktails_by_integredients,
    mocked_get_cocktails_by_ingredients, 
    input_args, 
    limit,
    printed_output
    ):
    get_cocktails_by_integredients.return_value = mocked_get_cocktails_by_ingredients
    capturedOutput = io.StringIO()
    sys.stdout = capturedOutput
    main(input_args, limit)
    sys.stdout = sys.__stdout__   # Reset redirect.
    assert capturedOutput.getvalue() == printed_output
